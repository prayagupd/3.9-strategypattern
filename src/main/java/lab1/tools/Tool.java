package lab1.tools;

import java.awt.*;
import lab1.ScribbleCanvas;

public interface Tool {
  void mousePressed(Point p, ScribbleCanvas canvas);
  void mouseReleased(Point p, ScribbleCanvas canvas); 
  void mouseDragged(Point p, ScribbleCanvas canvas); 
}
