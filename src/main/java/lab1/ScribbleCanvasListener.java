package lab1;

import java.awt.*;
import java.awt.event.*;
import lab1.tools.Tool;

public class ScribbleCanvasListener 
    implements MouseListener, MouseMotionListener {
        
  protected Lab1 drawframe;
  protected ScribbleCanvas canvas; 
  protected int xStart, yStart; 
  protected Graphics onscreen, offscreen; 

  public ScribbleCanvasListener(ScribbleCanvas canvas, Lab1 drawframe) {
    this.canvas = canvas;
    this.drawframe=drawframe;
  }

  public void mousePressed(MouseEvent e) {
    Tool tool = drawframe.getCurrentTool();
    if (tool != null) {
      tool.mousePressed(e.getPoint(), canvas); 
    }    
  }  
  
  public void mouseReleased(MouseEvent e) {
    Tool tool = drawframe.getCurrentTool(); 
    if (tool != null) {
      tool.mouseReleased(e.getPoint(), canvas); 
    }
  }
  
  public void mouseDragged(MouseEvent e) {
    Tool tool = drawframe.getCurrentTool(); 
    if (tool != null) {
      tool.mouseDragged(e.getPoint(), canvas); 
    }
  }

  public void mouseClicked(MouseEvent e) {}
  public void mouseEntered(MouseEvent e) {}  
  public void mouseExited(MouseEvent e) {}
  public void mouseMoved(MouseEvent e) {}     

  
}

